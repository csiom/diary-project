<nav class="navbar navbar-default " role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="">Home</a>
      	
        </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-left">
     
     <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Diary Panel <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
			  
            <li> <form method="POST" action="" accept-charset="UTF-8">  
						<input type='hidden' name='write_diary' value='write_diary' />
						<button type="submit" name="submit" class='btn btn-block btn-info' >Write Diary</button>
						</form>
        <li>   <br> </li>
            <li> <form method="POST" action="" accept-charset="UTF-8">  
						<input type='hidden' name='read_diary' value='read_diary' />
						<button type="submit" name="submit" class='btn btn-block btn-info' >Read Diary</button>
						</form></li>
          </ul>
        </li>
      </ul>

      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><? echo $_SESSION['username']; ?> <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            
             <form method="POST" action="" accept-charset="UTF-8">
              <input type='hidden' name='change_password' value='change_password' />          
              <button type="submit" name="submit" class="btn btn-block btn-info"><i class="icon-off icon-white"></i> Change Password</button>
            </form>
            <li class="divider"></li>
             <form method="POST" action="" accept-charset="UTF-8">
              <input type='hidden' name='logmeout' value='logmeout' />          
              <button type="submit" name="submit" class="btn btn-block btn-info"><i class="icon-off icon-white"></i> Logout</button>
            </form>

            </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!--<div class='container hero-unit'>
    <h2>Hello here's the secret content!</h2>
    <p>Check out the change in the navbar! Use the new  button to do just that. </p>
    </div>-->

</body>
</html>
