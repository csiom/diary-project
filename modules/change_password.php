<?php

echo "<div class='container hero-unit' > ";

if(isset($_POST['change_password'])) {
	
?>
<h1>Change Password</h1> <br /><br />
    <form action="" method="post"> 
        
        <table  class="table ">
			<tr><th><label>Current Password</label> </th><td><input type="password" name="current_password"  /></td></tr>
			<tr><th><label>New Password</label> </th><td><input type="password" name="new_password"  /></td></tr>
			<tr><th><label>Repeat Password</label> </th><td><input type="password" name="repeat_password"  /></td></tr>
			<tr><th colspan='2'><input type="submit" class="btn btn-info" value="Submit" /></th> </tr>
        
        </table>
        
        
        <input type="hidden"  name="submit_change_password" value="submit_change_password" /> 
    </form>
<?php
}

if(isset($_POST['submit_change_password'])) {
	
	// Ensure that the user fills out fields 
    if(empty($_POST['current_password'])) 
    {  echo "<p class='alert alert-info'>Please enter Current Password.</p>"; } 
    if(empty($_POST['new_password'])) 
    {  echo "<p class='alert alert-info'>Please enter New password.</p>"; } 
    if($_POST['new_password']!=$_POST['repeat_password'])
    { echo "<p class='alert alert-info'>Password Miss Match</p>"; 
		break;
	}
    
	if(!empty($_POST)){ 
        $query = " 
            SELECT 
                * 
            FROM users 
            WHERE 
                username = :username 
        "; 
        $query_params = array( 
            ':username' => $_SESSION['username']
        ); 
         
        try{ 
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex){ die("Failed to run query: " . $ex->getMessage()); } 
        $login_ok = false; 
        $row = $stmt->fetch(); 
        if($row){ 
            $check_password = hash('sha256', $_POST['current_password'] . $row['salt']); 
            for($round = 0; $round < 65536; $round++){
                $check_password = hash('sha256', $check_password . $row['salt']);
            } 
            if($check_password === $row['password']){
                $login_ok = true;
            } 
        }
        if($login_ok){  
			$query = " UPDATE `users` SET password=:password, salt=:salt  WHERE username = :username";    
			// Security measures
			$salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647)); 
			$password = hash('sha256', $_POST['new_password'] . $salt); 
			for($round = 0; $round < 65536; $round++){ $password = hash('sha256', $password . $salt); } 
			$query_params = array( 
				':password' => $password, 
				':salt' => $salt,  
				':username' => $_SESSION['username']  
			); 
			try {  
				$stmt = $db->prepare($query); 
				$result = $stmt->execute($query_params); 
			} 
			catch(PDOException $ex){ die("Failed to run query: " . $ex->getMessage()); } 
		 echo "<p class='alert alert-info'>Password successfuly changed. </p>";
        } 
        else{ 
         echo "<p class='alert alert-info'>Incorret Current Password.</p>";
         }
    }
}	

?>
</div>
