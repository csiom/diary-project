<?php 
date_default_timezone_set('Asia/Kolkata');

session_start(); 

include_once("config/config.php"); 
include_once("config/header.php"); 
include_once("modules/function.php"); 

if(isset($_POST['username']) && isset($_POST['password'])) {	
$submitted_username = ''; 
    if(!empty($_POST)){ 
        $query = " 
            SELECT 
                * 
            FROM users 
            WHERE 
                username = :username 
        "; 
        $query_params = array( 
            ':username' => $_POST['username']
        ); 
         
        try{ 
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex){ die("Failed to run query: " . $ex->getMessage()); } 
        $login_ok = false; 
        $row = $stmt->fetch(); 
        if($row){ 
            $check_password = hash('sha256', $_POST['password'] . $row['salt']); 
            for($round = 0; $round < 65536; $round++){
                $check_password = hash('sha256', $check_password . $row['salt']);
            } 
            if($check_password === $row['password']){
                $login_ok = true;
            } 
        } 
        if($login_ok){ 
            unset($row['salt']); 
            unset($row['password']); 
            $_SESSION['user'] = $row;  
            $_SESSION['usertype'] = $row['usertype'];  
            $_SESSION['username'] = $row['username'];  
            include_once('main.php');
         #   header("Location: main.php"); 
           # die("Redirecting to: main.php"); 
           
        } 
        else{ 
			include_once('login.php');
           echo "<p class='alert alert-info'> Login Failed  </p>";         
           } 
    } 
}


elseif(isset($_POST['logmeout'])) 
{
		session_destroy();
		include_once('login.php');
}

elseif(isset($_SESSION['usertype']) && isset($_SESSION['username'])) 
{
	if(!isset($_POST['logmeout'])) {
		include_once('main.php');
		}
	
	if(isset($_POST['write_diary'])) {
			
			include_once('modules/employee/write_diary.php');
		}
	
	if(isset($_POST['change_password'])) {
			
			include_once('modules/change_password.php');
		}
	
	if(isset($_POST['submit_change_password'])) {
			
			include_once('modules/change_password.php');
		}
	
	
	if(isset($_POST['write_diary_submit'])) {
			
			include_once('modules/employee/write_diary.php');
		}
		
	if(isset($_POST['read_diary'])) {
			
			include_once('modules/employee/read_diary.php');
		}
		
	if(isset($_POST['comment_submit'])) {
			
			include_once('modules/employee/read_diary.php');
		}
	
	if(isset($_POST['employee_diary'])) {
			
			include_once('modules/manager/employee_diary.php');
		}
	
	if(isset($_POST['username_submit'])) {
			
			include_once('modules/employee/read_diary.php');
		}
			
	else {
		include_once('main.php');
	}
		
}
else
{
include_once("login.php");
}
?>
