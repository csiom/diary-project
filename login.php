<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Diary Project</a>		
        </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      
        <!--<li><a href="#">Write</a></li>-->
        
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
     <li><a href="register.php">Register</a></li>
     <li class="divider-vertical"></li>
        <li class="dropdown">
			 <a class="dropdown-toggle" href="#" data-toggle="dropdown"> Log In <strong class="caret"></strong></a>
            <div class="dropdown-menu" style="padding: 10px; padding-bottom: 5px; width:250px">
			<form action="" method="post"> 
                     Username:
                    <input type="text" class="form-control"  name="username" value="" /> 
                    <br /> 
                    Password:<br /> 
                    <input type="password" class="form-control"  name="password" value="" /> 
                    <br /><br /> 
                    <input type="submit" class="btn btn-info" value="Login" /> 
                </form> 
            </div>	
		 </li>
        </ul>		 
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



<div class="container hero-unit">
    <h1>Welcome</h1>
    <p>The Whole new diary writing experience !</p>
    <h2>Way you can log in:</h2>
    <ul>
        <li>Try out your own user + password with the <strong>Register</strong> button in the navbar.</li>
    </ul>
</div>

</body>
</html>
